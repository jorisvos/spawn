package com.jorisvos.spawn;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class MainClass extends JavaPlugin {
	ConsoleCommandSender console = Bukkit.getConsoleSender();
	
	public void onEnable() { console.sendMessage(ChatColor.LIGHT_PURPLE + "[Spawn] " + ChatColor.GREEN + "Spawn version V"+getDescription().getVersion()+" is enabled!"); }
	public void onDisable() { console.sendMessage(ChatColor.LIGHT_PURPLE + "[Spawn] " + ChatColor.RED + "Spawn version V"+getDescription().getVersion()+" is disabled!"); }
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (cmd.getName().equalsIgnoreCase("spawn"))
		{
			if (!(sender instanceof Player))
				sender.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "[Spawn] " + ChatColor.RED + "You have to be a player to execute this command!");
			else
			{
				Player player = (Player)sender;
				
				if (getConfig().isSet("worlds."+player.getWorld()))
				{
					player.teleport(getLocationFromConfig(player.getWorld()));
					player.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "[Spawn] " + ChatColor.GREEN + "Spawn!");
				}
				else
					player.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "[Spawn] " + ChatColor.BLUE + "The spawn is not set in this world!");
			}
			
			return true;
		}
		else if (cmd.getName().equalsIgnoreCase("setspawn"))
		{
			if (!(sender instanceof Player))
				sender.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "[Spawn] " + ChatColor.RED + "You have to be a player to execute this command!");
			else
			{
				Player player = (Player)sender;
				saveLocationToConfig(player.getLocation(), player.getWorld());
				player.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "[Spawn] " + ChatColor.GREEN + "Spawn set!");
			}
			
			return true;
		}
		
		return false;
	}
	
	public Location getLocationFromConfig(String worldName) { return getLocationFromConfig(Bukkit.getWorld(worldName)); }
	public Location getLocationFromConfig(World world)
	{
		Double x = getConfig().getDouble("worlds."+world+".x");
		Double y = getConfig().getDouble("worlds."+world+".y");
		Double z = getConfig().getDouble("worlds."+world+".z");
		
		Integer yaw = getConfig().getInt("worlds."+world+".yaw");
		Integer pitch = getConfig().getInt("worlds."+world+".pitch");

		return new Location(world, x, y, z, yaw, pitch);
	}
	
	public Boolean saveLocationToConfig(Location location, World world)
	{			
		getConfig().set("worlds."+world+".x", Double.valueOf(location.getX()));
		getConfig().set("worlds."+world+".y", Double.valueOf(location.getY()));
		getConfig().set("worlds."+world+".z", Double.valueOf(location.getZ()));
			
		getConfig().set("worlds."+world+".yaw", Double.valueOf(location.getYaw()));
		getConfig().set("worlds."+world+".pitch", Double.valueOf(location.getPitch()));
			
		saveConfig();			
		return true;
	}
}
